from bottle import route, run, template, error

def preberi_kategorije():
    with open('./data/kategorije.txt', encoding='utf-8') as f:
        return {k: v for k, v in enumerate(x.strip() for x in f.readlines())}

def preberi_izdelke(kat=None):
    ret = {}
    id_ = 0
    with open('./data/izdelki.txt', encoding='utf-8') as f:
        while True:
            ime = f.readline().strip()
            if ime == '':
                break
            kategorija = int(f.readline())
            cena = float(f.readline())
            zaloga = int(f.readline())
            opis = f.readline().strip()
            if (kat is None) or (kategorija == kat):
                ret[id_] = {'ime': ime, 'kategorija': kategorija, 'cena': cena, 'zaloga': zaloga, 'opis': opis}
            id_ += 1
    return ret

@route('/kategorija/<kat:int>')
def prikaz_izdelkov(kat):
    return template('kategorija', ime=preberi_kategorije()[kat], izdelki=preberi_izdelke(kat))

@route('/')
def prikaz_kategorij():
    return template('home', kategorije=preberi_kategorije())

"""
@error(404)
def error_404(err):
    return 'Napaka 404!'
"""

run(host='localhost', port=8080, debug=True, reloader=True)
